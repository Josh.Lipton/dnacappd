FROM registry.gitlab.com/josh.lipton/base
# upgrade pip and install required python packages

ENV DNAC_URL='Null'
ENV DNAC_USERNAME='admin'
ENV DNAC_PASSWORD='NULL'
ENV DNAC_API_DEBUG=FALSE
ENV LOGGING_LEVEL=ERROR
ENV DNAC_VERSION=1.3.0
ENV LOG_DIR='/opt/logs'
ENV OUTPUT='/opt/data'
ENV REFRESH='600'
ENV APPD-EVENTS-API-ACCOUNTNAME='NULL'
ENV APPD-EVENTS-API-KEY='NULL'
ENV APPD_URL='NULL"

#curl --header "PRIVATE-TOKEN: x1iRYZsG45Mrv_DsQipf" https://gitlab.com/api/v4/projects/16706667/repository/archive
WORKDIR /opt/app
RUN curl https://gitlab.com/api/v4/projects/16813605/repository/archive.tar.gz -O
RUN tar -zvxf /opt/app/archive.tar.gz -C /opt/app --strip-components 1
RUN pip install -r /opt/app/requirements.txt

WORKDIR /opt/app
RUN python setup.py install
WORKDIR /opt
#CMD dnacsync tasks start




