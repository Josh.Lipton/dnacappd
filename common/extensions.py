import logging,os

def startlogging(loglevel):
	if 'LOG_DIR' in os.environ.keys():
		script_dir = os.environ['LOG_DIR']
		abs_file_path = os.path.join(script_dir, 'app.log')
	else:
		script_dir = os.path.dirname('./')
		abs_file_path = os.path.join(script_dir, 'logs/app.log')

	logging.basicConfig (filename=abs_file_path, level=getattr(logging,loglevel.upper()), format='%(asctime)s %(levelname)s %(message)s',filemode='w')
	logging.debug ('Local Development Logging Enabled')