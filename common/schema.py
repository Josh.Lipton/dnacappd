

def updateKeys(data):
    ints = {'overallHealth', "memoryScore",'cpuScorer','overallHealth','wqeScore','clientCount','freeMbufScore','packetPoolScore','packetPoolScore','freeTimerScore'}
    floats = ('memory', 'cpu')
    strs = ('lastBootTime','ringStatus')
    for item in data:
            try:
                if (item in ints):
                    data[item] = int(data[item])
                elif (item in floats):
                    data[item] = float(data[item])
                elif (item in strs):
                    data[item]=str(data[item])
                else:
                    pass
            except:
                pass  # Returns result on next line.
    return data

