import os, json,time,sys
from common.extensions import logging


def openJson(file):
    try:
        logging.debug("File: " + str(file))
        with open(file) as f:
            data = json.load(f)
        return data
    except Exception as e:
        logging.error(str(e))
        return False

def openfile(file):
    script_dir = os.path.dirname('/')
    logging.debug("Script Dir:  " + str(script_dir))
    abs_file_path = os.path.join(script_dir, file)
    logging.debug("Absulate File Path:  " + str(abs_file_path))
    data = openJson(abs_file_path)
    logging.debug("Data Returned: " + str(data))
    if data == False:
        logging.error("Error with reading Json File, returning False to exit app")
        return False
    else:
        return data

def writefile(data):
    dir = os.environ['OUTPUT']
    abs_file_path = os.path.join(dir,'info.json')
    try:
        with open(abs_file_path,'w') as f:
            json.dump(data,f,sort_keys=True,
                 indent=4, separators=(',', ': '))
        sys.stdout.write('Time of Info JSON Update:' + time.strftime('%m/%d/%Y %H:%M:%S', time.localtime())+"\n")
        logging.info('Time of Info JSON Update:' + time.strftime('%m/%d/%Y %H:%M:%S', time.localtime()))
    except Exception as e:
        sys.stdout.write('Info JSON Update Failed: ' + time.strftime('%m/%d/%Y %H:%M:%S', time.localtime()) +"\n"
                         +"Error Message: " + str(e))
        logging.error("Error Writing Info File: " + str(e))
