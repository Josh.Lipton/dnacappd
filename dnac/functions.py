import json
import datetime
import time
import sys
import logging
import json
from common.schema import updateKeys
def waitForTask(dnac, task):
    try:
        # print('Wait For Task: '+ str(task))
        if 'executionStatusUrl' in task.keys():
            r = dnac.custom_caller.call_api('get', resource_path=task[
                'executionStatusUrl'])
            status = str(r['status'])
            # print('executionStatusUrl 1: '+ str(r))
            while status != 'SUCCESS' and status != 'FAILURE':
                try:
                    r = dnac.custom_caller.call_api('get',
                                                    resource_path=task[
                                                        'executionStatusUrl'])
                    status = str(r['status'])
                except Exception as e:
                    print(str(e))
                    r = {}
                    r['status'] = 'FAILURE'
                    return r
            return r
        elif 'url' in task['response'].keys():
            r = dnac.custom_caller.call_api('get',
                                            resource_path=task['response'][
                                                'url'])
            # print('URL 1:' + str(r))
            time.sleep(10)
            r = dnac.custom_caller.call_api('get',
                                            resource_path=task['response'][
                                                'url'])
            # print('URL 2:'+  str(r))
            return r['response']
        else:
            return task
    except Exception as e:
        print(str(e))
        r = {}
        r['status'] = 'FAILURE'
        return r
def updateTimeStamp():
    current_time = datetime.datetime.now()  # use datetime.datetime.utcnow() for UTC time
    ten_minutes_ago = current_time - datetime.timedelta(minutes=10)

    ten_minutes_ago_epoch_ts = int(current_time.timestamp() * 1000)
    return ten_minutes_ago_epoch_ts

def DNACcollection(dnac,familys):

    alldevices=[]
    tstamp = updateTimeStamp()
    print(tstamp)
    runtime = time.strftime('%m/%d/%Y %H:%M:%S', time.localtime())
    #Switches and Hubs
    for family in familys:
        devices = dnac.devices.get_device_list(family=family).response

        for device in devices:
            #print(device)
            if family == "Wireless Controller":
                try:
                    detail = dnac.devices.get_device_detail('uuid',device.id,timestamp=tstamp).response
                    detail = updateKeys(detail)
                    alldevices.append(detail)

                except Exception as e:
                    logging.error("Error reading DNAC APIs: " +str(e))
                    sys.stdout.write(
                        'Error reading DNAC APIs: ' + time.strftime(
                            '%m/%d/%Y %H:%M:%S', time.localtime()) + "\n"
                        + "Error Message: " + str(e) + "\n")
            else:
                try:
                    detail = dnac.devices.get_device_detail('uuid',device.id,timestamp=tstamp).response
                    detail = updateKeys(detail)
                    alldevices.append(detail)
                except Exception as e:
                    logging.error("Error reading DNAC APIs: " +str(e))
                    sys.stdout.write(
                        'Error reading DNAC APIs: ' + time.strftime(
                            '%m/%d/%Y %H:%M:%S', time.localtime()) + "\n"
                        + "Error Message: " + str(e) + "\n")
    #logging.debug(alldevices)
    #print(alldevices)
    logging.info (f"Task completed at: {time.strftime('%m/%d/%Y %H:%M:%S', time.localtime())}")
    return alldevices
