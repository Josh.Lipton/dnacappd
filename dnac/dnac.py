from common.extensions import logging
from dnacentersdk import api
def setup_custom(dnac):
    try:
        dnac.custom_caller.add_api('device_enrichment_details',
                                   lambda id:
                                   dnac.custom_caller.call_api('GET',
                                                               '/dna/intent/api/v1/device-enrichment-details',
                                                               headers={
                                                                   'entity_value': id,
                                                                   'entity_type': 'device_id'})
                                   ).response

    except Exception as e:
        logging.debug('device_enrichment_details error: ' + str(e))


def initDNAC(settings):
    try:
        dnac = api.DNACenterAPI(base_url=settings['url'], username=settings['username'],
                            password=settings['password'], verify=False, version=settings['version'],
                            debug=settings['debug'])
        setup_custom(dnac)

        return dnac
    except Exception as e:
        logging.debug('device_enrichment_details error: ' + str(e))



