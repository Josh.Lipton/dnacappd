import logging
import time
import threading
from dnac.dnac import initDNAC
from dnac.functions import DNACcollection
from common.fuctions import openfile,writefile
from appd.updateappd import update
from common.pid import Pid
import urllib3
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

class Deamon(Pid):
    #def __init__(self):
    #    self._running = True

    #def stop(self):
    #    self._running = False

    def run(self, *args):
    #def run(self,dnacconfig,refreshtime):
            dnacconfig= args[0]
            dnac = initDNAC(dnacconfig)
            refreshtime = args[1]
            #dnacsetting=openfile(dnacconfig)
            while True:
                logging.debug("Starting DNAC Tasks")
                devices = DNACcollection(dnac, dnacconfig['family'])
                writefile(devices)
                #print(devices)
                response = update(devices)
                time.sleep(refreshtime)

def startDaemon(dnacconfig,refreshtime):
    #d = Deamon()
    #t = threading.Thread(target=d.run,name='DNAC-SYNC',args=(dnacconfig,refreshtime,))
    #t.start()
    d = Deamon('/tmp/DNAC-SYNC.pid')
    d.start(dnacconfig,refreshtime)
    print('Sync Started')

def statusDaemon():
    d=Deamon('/tmp/DNAC-SYNC.pid')
    d.status()

def stopDaemon():
    d = Deamon('/tmp/DNAC-SYNC.pid')
    d.stop()
def restartDaemon():
    d = Deamon('/tmp/DNAC-SYNC.pid')
    d.restart()

#if __name__ == '__main__':
