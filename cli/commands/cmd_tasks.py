import os
import click
import time
from dnac.deamon import startDaemon,stopDaemon,statusDaemon,restartDaemon



@click.group()
def cli():
    """ Tasks to start sync of DNAC to AppD """
    pass

@click.command(help='Starts DNAC Assurance SYNC Task')
@click.option('--dnacUrl', default=lambda: os.environ.get('DNAC_URL'),
              show_default=os.environ.get('DNAC_URL'))
@click.option('--dnacUsername', default=lambda: os.environ.get('DNAC_USERNAME'),
              show_default=os.environ.get('DNAC_USERNAME'))
@click.option('--dnacPassword', default=lambda: os.environ.get('DNAC_PASSWORD'),
              show_default=os.environ.get('DNAC_PASSWORD'))
@click.option('--dnacApiDebug', default=lambda: os.environ.get('DNAC_API_DEBUG'),
              show_default=os.environ.get('DNAC_API_DEBUG'))
@click.option('--dnacVersion', default=lambda: os.environ.get('DNAC_VERSION'),
              show_default=os.environ.get('DNAC_VERSION'))
@click.option('--loggingLevel', default=lambda: os.environ.get('LOGGING_LEVEL'),
              show_default=os.environ.get('LOGGING_LEVEL'))
@click.option('--refresh', default=lambda: os.environ.get('REFRESH'),
              show_default=os.environ.get('REFRESH'))
def start(dnacurl,dnacusername,dnacpassword,dnacapidebug,dnacversion,logginglevel,refresh):

   dnacconfig = {
        "url": dnacurl,
        "username": dnacusername,
        "password": dnacpassword,
        "version":  dnacversion,
        "debug": dnacapidebug,
        "family": ["Wireless Controller","Routers","Switches and Hubs"]
    }

   startDaemon(dnacconfig,int(refresh))
   print('Deamon Started')
   #rungetinfo(dnacconfig)

@click.command(help='Stops DNAC Assurance SYNC Task')
def stop():
    stopDaemon()
    print('Deamon Stopped')

@click.command(help='Status DNAC Assurance SYNC Task')
def status():
    statusDaemon()
@click.command(help='Restarts DNAC Assurance SYNC Task')
def restart():
    restartDaemon()

cli.add_command(start)
cli.add_command(stop)
cli.add_command(status)
cli.add_command(restart)