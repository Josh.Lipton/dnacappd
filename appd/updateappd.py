import requests,logging,os,json

def update(data):

	Appd_headers={}
	Appd_headers['X-Events-API-AccountName'] = os.environ['APPD-EVENTS-API-ACCOUNTNAME']
	Appd_headers['X-Events-API-Key'] = os.environ['APPD-EVENTS-API-KEY']
	Appd_headers['Content-type']='application/vnd.appd.events+json;v=2'
	payload = json.dumps(data)

	try:
		update = requests.post(os.environ['APPD_URL'],headers=Appd_headers,json=data)
		logging.info(f"APPD Response: {update} Reason: {update.reason}")
		return True
	except Exception as e:
		logging.error(f"Update APPD error: {str(e)}")
		return False
