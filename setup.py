
"""
setup for click
"""
from setuptools import setup,find_packages

setup(
    name='dnacsync',
    version='1.001',
    author='Josh Lipton',
    author_email='joliptn@cisco.com',
    description='CLI Tool to Synce DNAC Assurance with Aood',
    #packages=[,'cli', 'cli.commands'],
    packages=find_packages(),
    include_package_data=True,
    install_requires=[
        'click',
        'dnacentersdk',
        'urllib3'
    ],
    entry_points="""
        [console_scripts]
        dnacsync=cli.cli:cli
    """,
)
